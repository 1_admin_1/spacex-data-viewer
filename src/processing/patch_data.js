import _set from 'lodash/set'
import _get from 'lodash/get'
const setFunc = (typeof _ !== 'undefined') ? _.set : _set
const getFunc = (typeof _ !== 'undefined') ? _.get : _get

export const standartPatch = (obj, patchData) => {
  patchData.forEach(({flight, patch}) => {
    patch.forEach(p => {
      setFunc(obj[flight - 1], p.field, p.value)
      if (p.field.endsWith('payload_mass_kg')) {
        setFunc(obj[flight - 1], p.field.replace('payload_mass_kg', 'payload_mass_lbs'), Math.trunc(p.value * 2.20462))
      }
    })
  })
}

export const allPatch = (obj, patchData) => {
  standartPatch(obj, patchData)
  removeFalcon1(obj)
  return obj
}

function removeFalcon1(obj) {
  obj.splice(0, 5)
  obj.forEach(v => { v.flight_number -= 5 })
}

export const futurePatch = (obj, patchData) => {
  patchData.forEach(patchFlight => {
    if ('name' in patchFlight) {
      let flight = patchFlight.name === 'ALL' ? obj : obj.filter(i => i.rocket.second_stage.payloads[0].payload_id === patchFlight.name)
      flight.forEach(f =>
        patchFlight.patch.forEach(p => {
          if ((!('old' in p)) || (p.old === getFunc(f, p.field))) { setFunc(f, p.field, p.value) }
        }))
    }
  })
  obj.forEach(v => { if (Number.isInteger(v.flight_number)) v.flight_number -= 5 })
  return obj
}
