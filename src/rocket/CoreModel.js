export default class CoreModel {
  constructor(serial, version) {
    this.serial = serial
    this.version = version
    this.data = null
    this.missions = []
    this.launchCount = 0
    this.launchSuccessCount = 0
    this.land = {RTLS: 0, All: 0, ASDS: 0, Ocean: 0, Ret: 0}
    this.firstLaunchDate = null
    this.lastLaunchDate = null
  }

  addFlight({core, launch}) {
    this.missions.push(Mission.createFromFlight({core, launch}))
    if (!launch.future) {
      this.launchCount += 1
      this.launchSuccessCount += launch.launchSuccess ? 1 : 0
      if (core.land_success) {
        this.land[core.landing_type] += 1
        this.land.Ret = this.land.ASDS + this.land.RTLS
        this.land.All = this.land.Ret + this.land.Ocean
      }
      if (this.firstLaunchDate == null) { this.firstLaunchDate = launch.date }
      this.lastLaunchDate = launch.date
    }
  }

  static createFromFlights(flights) {
    let coreModel = new CoreModel(flights[0].core.core_serial, flights[0].core.version)
    flights.forEach(i => coreModel.addFlight(i))
    return coreModel
  }
}

class Mission {
  constructor(name, flightNumber, style) {
    this.name = name
    this.flightNumber = flightNumber
    this.status = style
  }

  static createFromFlight({launch, core}) {
    return new Mission(launch.mission, launch.flightNumber, Mission.styleFromFlight({launch, core}))
  }

  toString() {
    return this.name
  }

  static styleFromFlight({launch, core}) {
    if (launch.future === true) return 'light'
    if (launch.launchSuccess === false) return 'danger'
    if (core.landing_type === null) return 'outline-dark'
    if (core.land_success === false) return 'outline-danger'
    if (core.landing_type === 'RTLS') return 'success'
    if (core.landing_type === 'ASDS') return 'info'
    if (core.landing_type === 'Ocean') return 'outline-info'
  }
}
