export default class FalconBasic {
  constructor(obj, future = false) {
    this.data = obj
    this.future = future
  }

  get reused() {
    return this.coresFlight.filter(i => i > 1).length > 0
  }

  get mission() {
    return this.data.mission_name
  }

  set mission(value) {
    this.data.mission_name = value
  }

  get payloads() {
    return this.data.rocket.second_stage.payloads
  }

  set payloads(value) {
    this.data.rocket.second_stage.payloads = value
  }

  get type() {
    return types.get(this.rocket.rocket_id)
  }

  get cores() {
    return this.rocket.first_stage.cores
  }

  set cores(value) {
    this.rocket.first_stage.cores = value
  }

  get core() {
    return this.rocket.first_stage.cores[0]
  }

  set core(value) {
    this.rocket.first_stage.cores[0] = value
  }

  get rocket() {
    return this.data.rocket
  }

  set rocket(value) {
    this.data.rocket = value
  }

  get flight() {
    return this.data.flight_number
  }

  get flightNumber() {
    return this.data.flight_number
  }

  set flightNumber(value) {
    this.data.flight_number = value
  }

  get date() {
    if (this.data.launch_date_utc !== null) { return this.data.launch_date_utc.slice(0, 10) }
  }

  get dateFuture() {
    if (this.data.launch_date_utc !== null) {
      if (this.data.tentative_max_precision === 'year') { return this.data.launch_date_utc.slice(0, 4) }
      if (this.data.tentative_max_precision === 'month') { return this.data.launch_date_utc.slice(0, 7) }
      if (this.data.tentative_max_precision === 'half') {
        const half = Number(this.data.launch_date_utc.slice(5, 7)) > 6 ? 'H2' : 'H1'
        return this.data.launch_date_utc.slice(0, 5) + half
      }
      if (this.data.tentative_max_precision === 'quarter') {
        const quartal = Math.floor((Number(this.data.launch_date_utc.slice(5, 7)) + 2) / 3)
        return this.data.launch_date_utc.slice(0, 5) + 'Q' + quartal
      }
      return this.data.launch_date_utc.slice(0, 10)
    }
  }

  get dateFuturePseudoForSort() {
    if (this.data.launch_date_utc !== null) {
      if (this.data.tentative_max_precision === 'year') { return this.data.launch_date_utc.slice(0, 4) + '-X' }

      if (this.data.tentative_max_precision === 'half') {
        const endMonth = Number(this.data.launch_date_utc.slice(5, 7)) > 6 ? 12 : 6
        return this.data.launch_date_utc.slice(0, 5) + ('00' + endMonth).slice(-2) + '-34'
      }
      if (this.data.tentative_max_precision === 'quarter') {
        const quartal = Math.floor((Number(this.data.launch_date_utc.slice(5, 7)) + 2) / 3)
        return this.data.launch_date_utc.slice(0, 5) + ('00' + quartal * 3).slice(-2) + '-33'
      }
      if (this.data.tentative_max_precision === 'month') { return this.data.launch_date_utc.slice(0, 7) + '-32' }
      return this.data.launch_date_utc.slice(0, 10)
    }
  }

  get staticFireDate() {
    if (this.data.static_fire_date_utc !== null && this.data.launch_date_utc !== this.data.static_fire_date_utc) {
      return this.data.static_fire_date_utc.slice(0, 10)
    }
  }

  get launchSuccess() {
    return this.data.launch_success
  }

  get orbit() {
    return this.payloads[0].orbit
  }

  // ----- Payloads

  set orbit(value) {
    this.payloads[0].orbit = value
  }

  get payloadsMass() {
    return this.payloads.map(i => i.payload_mass_kg).filter(i => i !== null)
  }

  set firstPayloadMass(value) {
    this.payloads[0].payload_mass_kg = value
  }

  get payloadsMassWithShips() {
    return [...this.payloads.map(i => i.payload_mass_kg),
      this.massShips]
      .filter(i => (i !== null) && (i !== 0))
  }

  get fullMass() {
    let mass = this.payloads
      .map(i => i.payload_mass_kg)
      .filter(i => i !== null)
      .reduce((i1, i2) => i1 + i2, 0)
    return isNaN(mass) ? '' : mass
  }

  get fullMassWithShips() {
    return this.fullMass + (shipsMass.has(this.payloads[0].payload_type) ? shipsMass.get(this.payloads[0].payload_type) : 0)
  }

  get massShips() {
    return (shipsMass.has(this.payloads[0].payload_type) ? shipsMass.get(this.payloads[0].payload_type) : 0)
  }

  get payloadNames() {
    return this.payloads.map(i => i.payload_id)
  }

  get landingStatus() {
    return this.cores.map(i => i.land_success)
  }

  get landExpected() {
    return this.core.landing_type !== null
  }

  get landingType() {
    if (this.cores.length === 0 || (!('landing_type' in this.core))) return ['-']
    return this.cores.map(i => i.landing_type === null ? (this.future && (i.landing_intent !== false) ? 'Unknown' : 'No Attempt') : (landingName.get(i.landing_type) + (i.landing_vehicle !== null ? ' (' + i.landing_vehicle + ')' : '')))
  }

  get landingShort() {
    return this.cores.map(i => i.landing_type === null ? (this.future && (i.landing_intent !== false) ? '-' : 'No') : (landingName.get(i.landing_type)))
  }

  get landingVariant() {
    return {short: this.landingShort, full: this.landingType}
  }

  get coresSerial() {
    return this.cores.map(i => i.core_serial)
  }

  get coresVersion() {
    return this.cores.map(i => i.version)
  }

  get coresFlight() {
    return this.cores.map(i => i.flight)
  }

  get typeAndOrbit() {
    return this.type + '/' + this.orbit
  }

  get mass() {
    return this.fullMass
  }

  // short name for search

  get massWithShips() {
    return this.fullMassWithShips
  }

  get version() {
    return this.coresVersion
  }

  get serial() {
    return this.coresSerial
  }

  get payload() {
    return this.payloadNames
  }

  get year() {
    return this.data.launch_year
  }

  get land() {
    return this.landingStatus
  }

  prepare() {
    if (this.data.rocket.rocket_type.startsWith('v')) this.cores.forEach(core => { core.version = this.data.rocket.rocket_type })
    else if (this.data.rocket.rocket_type === 'FT') this.cores.forEach(i => { i.version = 'v1.2.' + (i.block || 'x') })
    return this
  }

  toJSON() {
    return this.data
  }
}

let types = new Map()
types.set('falcon1', 'F1')
types.set('falcon9', 'F9')
types.set('falconheavy', 'FH')

let shipsMass = new Map()
shipsMass.set('Dragon 1.0', 4200)
shipsMass.set('Dragon 1.1', 4200)

let landingName = new Map()
landingName.set('RTLS', 'Ground')
landingName.set('ASDS', 'Drone')
landingName.set('Ocean', 'Ocean')
