import Falcon from './Falcon.js'

export default class FalconTableViewer extends Falcon {
  constructor(obj, future = false) {
    super(obj, future)
  }

  prepare() {
    super.prepare()
    this.rowColor()
    this.landingColor()
    this._showDetails = false
    if (this.reused) this._cellVariants = Object.assign(this._cellVariants, {coresFlight: 'reused'})
    if (this.data.reuse.capsule) this._cellVariants = Object.assign(this._cellVariants, {payloadNames: 'reused-caps'})
    return this
  }

  rowColor() {
    if (this.launchSuccess === false) {
      this._rowVariant = 'danger'
    }
  }

  landingColor() {
    this._cellVariants = {landingStatus: this.landingColorInner()}
  }

  landingColorInner() {
    if (this.future) return 'default'
    let count = this.cores.filter(i => i.landing_type != null).length
    let countSuccess = this.cores.filter(i => i.land_success === true).length
    if (count === 0) { return 'secondary' }
    if (countSuccess === 0) { return 'danger' }
    if (countSuccess === count) {
      return (this.cores.filter(i => i.landing_type === 'Ocean').length > 0) ? 'info' : 'success'
    }
    return 'warning'
  }
}
