import Vue from 'vue'
import Router from 'vue-router'
import CoreTab from '@/components/Core'
import LaunchTab from '@/components/Launch'
import Stats from '@/components/Stats'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Launch',
      component: LaunchTab

    },
    {
      path: '/cores',
      name: 'Cores',
      component: CoreTab
    },
    {
      path: '/stats',
      name: 'Stats',
      component: Stats
    }
  ]
})
