// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
/* eslint-disable no-new */
import Vue from 'vue'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import TreeView from 'vue-json-tree-view'
import VueRouter from 'vue-router'
import App from './App'

import router from './router'
import {decodeToFilter} from './other/FilterConvert'

Vue.use(TreeView)
Vue.use(BootstrapVue)
Vue.use(VueRouter)

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (Object.keys(to.query).length === 0) {
    document.title = to.name
  } else {
    document.title = to.name + ': ' + decodeToFilter(to, ' ')
  }
  next()
})

if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('/sw.js')
  })
}

new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
})
