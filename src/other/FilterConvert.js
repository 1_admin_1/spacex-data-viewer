const delimeters = ['=', '?', '>', '<', '^']
const type = [null, 'in', 'gt', 'lt', 'st']

export const encodeToQuery = (filter) => {
  let value = filter
  if ((value === null || value.length === 0)) return {query: {}}
  if (value.includes('&')) return {query: {filters: value}}
  for (let i in delimeters) {
    let del = delimeters[i]
    if (value.includes(del)) {
      let res = value.split(del)
      let query = {}
      query[res[0]] = res[1]
      if (type[i] !== null) query.op = type[i]
      return {query}
    }
  }
  return {query: {filter: value}}
}

export const decodeToFilter = ({query}, sp = '') => {
  if ('filters' in query) return query.filters
  let filterKeyValue = Object.entries(query).filter((i, j) => i !== 'op')
  if (filterKeyValue.length === 0) return ''
  if (filterKeyValue[0][0] === 'filter') return filterKeyValue[0][1]
  if (!('op' in query)) {
    return filterKeyValue[0].join(sp + '=' + sp)
  }

  let ind = type.indexOf(query.op)
  return filterKeyValue[0].join(sp + (ind > -1 ? delimeters[ind] : '=') + sp)
}
