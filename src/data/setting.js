import Vue from 'vue'
import VueLocalStorage from 'vue-localstorage'
import {sourceFutureLaunchVariant, sourceLaunchVariant} from './variant'
let setting = {
  defaultsort: true,
  masswithships: false,
  leoorbitaggregate: [1, 2],
  itempage: 50,
  limitfuturerows: 5,
  mobileview: true,
  viewnewcores: true,
  viewfuturelaunchincores: 2,
  reusedincolor: false,
  sourcelaunch: sourceLaunchVariant.spacexdata,
  sourceupcominglaunch: sourceFutureLaunchVariant.spacexdata,
  heightchart: 28,
  chart2x: false,
  version: 0,
  patch: false,
  patch_available: false,
  snapshot_available: false,
  markreusedcores: 'flag',
  markreusedcaps: 'flag'
}

/* Default setting
  User settings are stored in localStorage.
  For change default values create in root setting.json with same key and new value, and rebuild app.
  For reset concrete field setting in localStorage,
  increment version number in setting.json and add new field with name 'r'+(new version number) and values - key list.
  ---
  "version": 6,
  "r6": ["chart2x","heightchart","sourcelaunch"]
  ---
  This fields reset to default values.
*/

try {
  let customSetting = require('../../setting.json')
  Object.entries(customSetting).forEach(([key, value]) => { setting[key] = value })
} catch (e) {}

Vue.use(VueLocalStorage, {
  bind: true
})

export default new Vue({
  beforeCreate() {
    let current = Vue.localStorage.get('version', setting.version, Number)
    while (current < setting.version) {
      let fields = setting['r' + (++current)]
      if (fields !== undefined) {
        fields.forEach(field => {
          console.log('Change setting ' + field + ' to default:' + setting[field])
          Vue.localStorage.set(field, setting[field])
        })
      }
    }
    Vue.localStorage.set('version', setting.version)
  },
  localStorage: {
    defaultsort: {
      type: Boolean,
      default: setting.defaultsort
    },
    masswithships: {
      type: Boolean,
      default: setting.masswithships
    },
    leoorbitaggregate: {
      type: Array,
      default: setting.leoorbitaggregate
    },
    itempage: {
      type: Number,
      default: 50
    },
    mobileview: {
      type: Boolean,
      default: setting.mobileview
    },
    viewnewcores: {
      type: Boolean,
      default: setting.viewnewcores
    },
    viewfuturelaunchincores: {
      type: Number,
      default: setting.viewfuturelaunchincores
    },
    reusedincolor: {
      type: Boolean,
      default: setting.reusedincolor
    },
    sourcelaunch: {
      type: Number,
      default: setting.sourcelaunch
    },
    sourceupcominglaunch: {
      type: Number,
      default: setting.sourceupcominglaunch
    },
    heightchart: {
      type: Number,
      default: setting.heightchart
    },
    chart2x: {
      type: Boolean,
      default: setting.chart2x
    },
    version: {
      type: Number,
      default: setting.version
    },
    patch: {
      type: Boolean,
      default: setting.patch
    },
    markreusedcores: {
      type: String,
      default: setting.markreusedcores
    },
    markreusedcaps: {
      type: String,
      default: setting.markreusedcaps
    },
    limitfuturerows: {
      type: Number,
      default: setting.limitfuturerows
    }
  },
  watch: {
    sourceupcominglaunch() {
      this.$emit('changeSources', 'upcoming')
    },
    sourcelaunch() {
      this.$emit('changeSources', 'launch')
    },
    patch() {
      this.$emit('changeSources', 'upcoming')
      this.$emit('changeSources', 'launch')
    }
  },
  data: {
    patch_available: setting.patch_available,
    snapshot_available: setting.snapshot_available
  },
  computed: {
    upcomingDisable() {
      return this.sourceupcominglaunch === sourceFutureLaunchVariant.disable
    }
  }

})
