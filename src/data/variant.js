export const sourceLaunchVariant = Object.freeze({'snapshot': 0, 'spacexdata': 1})
export const sourceFutureLaunchVariant = Object.freeze({'disable': 0, 'spacexdata': 1})
