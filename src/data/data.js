import Vue from 'vue'
import VueResource from 'vue-resource'
import Falcon from '../rocket/FalconTableViewer'
import CoreModel from '../rocket/CoreModel'
import setting from './setting'
import {allPatch, futurePatch} from '@/processing/patch_data.js'
import {sourceFutureLaunchVariant, sourceLaunchVariant} from './variant'
Vue.use(VueResource)

const urlData = '/static/data.json'
const urlPatch = '/static/patch.json'
const urlExamples = '/static/examples.json'
const urlSpXData = 'https://api.spacexdata.com/v2/launches'
const urlSpXDataUpcome = 'https://api.spacexdata.com/v2/launches/upcoming'
const urlSpXDataDragon = 'https://api.spacexdata.com/v2/parts/caps'

export default new Vue({
  created() {
    this.loadData()
    setting.$on('changeSources', this.changeSources)
  },
  data() {
    return {
      launch_data: [],
      upcoming_data: [],
      examples: [],
      setting: setting,
      dragon: [],
      patchPromise: null,
      loading_status: false
    }
  },
  methods: {
    loadData() {
      this.loadLaunchData()
      this.loadUpcomingData()
      this.loadExamples()
    },
    changeSources(source) {
      if (source === 'upcoming') this.loadUpcomingData()
      if (source === 'launch') this.loadLaunchData()
    },
    loadExamples() {
      this.$http.get(urlExamples).then(response => {
        this.examples = response.body
      })
    },
    loadDragon() {
      if (this.dragon.length === 0) {
        this.$http.get(urlSpXDataDragon).then(response => {
          this.dragon = response.body
        })
      }
    },
    getPatchPromise() {
      if (!this.setting.patch_available || !this.setting.patch) return Promise.resolve({future: [], past: []})
      if (this.patchPromise === null) {
        this.patchPromise = this.$http.get(urlPatch).then(response => {
          return response.body
        },
        () => { return {future: [], past: []} })
      }
      return this.patchPromise
    },
    loadUpcomingData() {
      if (this.setting.sourceupcominglaunch === sourceFutureLaunchVariant.disable) return
      let pr1 = this.$http.get(urlSpXDataUpcome).then(response => {
        return response.body
      })
      let pr2 = this.getPatchPromise()
      Promise.all([pr1, pr2]).then(([data, patch]) => {
        let tempData = futurePatch(data, patch.future)
        tempData = tempData.map(i => new Falcon(i, true).prepare())
        this.upcoming_data = _.sortBy(tempData, 'dateFuturePseudoForSort')
      })
    },
    loadLaunchDataThis() {
      return this.$http.get(urlData).then(response => {
        this.loading_status = false
        this.launch_data = response.body.map(i => new Falcon(i).prepare())
      }, () => { this.loading_status = false })
    },
    loadLaunchDataSpXData() {
      let pr1 = this.$http.get(urlSpXData).then(response => {
        this.loading_status = false
        return response.body
      }, () => { this.loading_status = false })
      let pr2 = this.getPatchPromise()

      return Promise.all([pr1, pr2]).then(([data, patch]) => {
        let tempData = allPatch(data, patch.past)
        this.launch_data = tempData.map(i => new Falcon(i).prepare())
      })
    },
    loadLaunchData() {
      this.loading_status = true
      if (!this.setting.snapshot_available) { this.loadLaunchDataSpXData(); return }
      if (this.setting.sourcelaunch === sourceLaunchVariant.snapshot) {
        this.loadLaunchDataThis().catch(i => this.loadLaunchDataSpXData())
      } else {
        this.loadLaunchDataSpXData().catch(i => this.loadLaunchDataThis())
      }
    },
    blockDataBySerial() {
      return _.groupBy(this.blockData, 'core.core_serial')
    }
  },
  computed: {
    blockData() {
      return _.flatMap(this.launch_data, i => i.cores.map(j => {
        return {core: j, launch: i}
      }))
    },
    blockDataByYear() {
      return _.groupBy(this.blockData, 'launch.data.launch_year')
    },
    blockDataBySerialWithStat() {
      let cores = this.blockDataBySerial()
      if (cores.length === 0) return []
      if (this.setting.viewfuturelaunchincores >= 1) {
        this.upcoming_data.forEach(flight => flight.cores.filter(i => !(i.core_serial === null)).forEach(i => {
          if (i.flight > 1 && (i.core_serial in cores)) {
            cores[i.core_serial].push({core: i, launch: flight})
          } else if (this.setting.viewfuturelaunchincores === 2) { cores[i.core_serial] = [{core: i, launch: flight}] }
        }))
      }
      let coresModel = Object.values(cores).map(i => CoreModel.createFromFlights(i))
      return _.sortBy(coresModel, 'serial')
    }
  }

})
