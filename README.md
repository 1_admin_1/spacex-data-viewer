# SpaceX Data Viewer

Viewer for launch data, core data, and different statistics

[https://spacexdataviewer.bitbucket.io](https://spacexdataviewer.bitbucket.io)

Webpack + Vue + ChartJS + Bootstrap-Vue

##Source Data
This project use data from SpaceX Data REST API [GitHub](https://github.com/r-spacex/SpaceX-API)

By default, app using directly SpaceX Data service (api.spacexdata.com) and local static patches (if available), 
with fallback to local snapshot data. 

Source don't include local snapshot data and patches. 

## Build / Run 

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# eslint report
npm run lint

# eslint autofix
npm run fix

# run light-server (if installed) with dist content
npm run server
```

License: MIT
